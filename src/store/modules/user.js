import Rest from '@/services/Rest.js'
export const namespaced = true


export const state = {
    userInfo: []
}

export const mutations = {
    ADD_USERINFO(state, event) {
        state.userInfo.push(event)
    }
   
  }

export const actions = {
    getUserList({ commit }, page) {
      return Rest.getUser(page).then((res) => {
          //console.log(res)
          commit("ADD_USERINFO", res)
          return res
      })
    },
    getUser({ commit }, id) {
      return Rest.getUserOne(id).then((res) => {
          //console.log(res)
          commit("ADD_USERINFO", res)
          return res
      })
    },
    getSearch({ commit }, query) {
      return Rest.getSearch(query).then((res) => {
          //console.log(res)
          commit("ADD_USERINFO", res)
          return res
      })
    },
    postUser({ commit }, user) {
      return Rest.postUser(user).then((res) => {
          //console.log(res)
          commit("ADD_USERINFO", res)
          return res
      })
    },
    postUpdateUser({ commit }, id, user) {
      return Rest.postUpdateUser(id, user).then((res) => {
          //console.log(res)
          commit("ADD_USERINFO", res)
          return res
      })
    }
  }

