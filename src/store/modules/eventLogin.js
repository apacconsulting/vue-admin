import Rest from "@/services/Rest.js";
import Utils from "@/shared/utils.js";
// import axios from "axios";
import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from '../actions/auth'
// import { USER_REQUEST } from '../actions/user'

export const namespaced = true;

export const state = {
  token: localStorage.getItem('TOKEN_ID') || '', status: '', hasLoadedOnce: false
}

export const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
}

export const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [AUTH_SUCCESS]: (state, resp) => {
    state.status = 'success'
    state.token = resp.data.data.tokenId
    state.hasLoadedOnce = true
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
  }
};

export const actions = {
  loginEvent : ({commit}, event) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)
      Rest.loginEvent(event)
      .then(resp => {
        Utils.setLocalStorage("TOKEN_ID", resp.data.data.tokenId);
        // Here set the header of your ajax library to the token value.
        // example with axios
        // axios.defaults.headers.common['Authorization'] = resp.token
        commit(AUTH_SUCCESS, resp);
        // dispatch(USER_REQUEST)
        resolve(resp);
      })
      .catch(err => {
        commit(AUTH_ERROR, err)
        localStorage.removeItem('TOKEN_ID')
        reject(err)
      })
    })
  },
  [AUTH_LOGOUT]: ({commit}) => {
    return new Promise((resolve) => {
      commit(AUTH_LOGOUT)
      localStorage.removeItem('TOKEN_ID')
      resolve()
    })
  }
  
};

