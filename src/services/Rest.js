import axios from 'axios'
import Utils from "@/shared/utils.js"

export const apiClient = axios.create({
  baseURL: `https://pacificgo.asia/admin/api/v1`,//http://localhost:9000/api/v1/`,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + Utils.getLocalStorage('TOKEN_ID')
  },
  timeout: 10000
})


export default {
  loginEvent(event) {
    return apiClient.post('/signIn', event)
  },
  getUser(page) {
    return apiClient.get('/user/list?paginate='+page)
  },
  getUserOne(id) {
    return apiClient.get('/user/'+ id)
  },
  getSearch(query) {
    return apiClient.get('/user/search/q?term='+ query)
  },
  postUser(user){
    return apiClient.post('/signUp', user)
  },
  postUpdateUser(id, user){
    return apiClient.put('/users/'+id, user)
  }
}
