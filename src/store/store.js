import Vue from "vue";
import Vuex from "vuex";
import * as eventLogin from "@/store/modules/eventLogin";
import * as user from "@/store/modules/user";
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        eventLogin,
        user
    }
});
