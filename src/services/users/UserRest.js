import Rest from '@/services/Rest.js';

export default {
    getUser() {
        return Rest.apiClient.get('/user/list');
    },
    getUserOne(id){
    	return Rest.apiClient.get('/user/'+ id);
    },
    getSearch(query) {
        return Rest.apiClient.get('/user/search/q?term='+ query);
    }
    postUser(user){
    	return Rest.apiClient.post('/signUp', user);
    }
    postUpdateUser(id, user){
    	return Rest.apiClient.put('/users/'+id, user);
    }
}